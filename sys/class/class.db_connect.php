<?php

    /* 
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    require_once '../sys/core/init.php';
    
    class database {

        /*Creates the method that will do the actual connection to the database.
         * This is the method that wants to make use of the varaibles defined in the
         * dbsettings file and is the reason why the file needs to be included into this file.
         */
        
        public function databaseConnect(){
            /*Creates a new instance of the PDO called $db.
             * NOTE: the use of DSN, DB_USER and so on.  These variable live in the dbsettings file.
            */
            $db = new PDO(DSN,DB_USER,DB_PASS);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $db;
        }
    }

?>