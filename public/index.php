<?php

//     ini_set('display_errors', 1);
//     error_reporting(E_ALL);
    ini_set('display_errors', 0);
    error_reporting(0);


    require_once '../sys/config/paths.php';
//    $loginDiv = checkLoggedIn(page::INDEX);

//    if($_POST || $_REQUEST || $_GET){
//        if(isCorrectHost() !== true){
//            logOut();
//            exit;
//        }
//    }

    
    $pageTitle = 'Home';
    $companyName = 'Kiwi Express';


?>

<html lang='en'>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!--  jQuery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>

    <!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
    <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

    <!-- Bootstrap Date-Picker Plugin -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

    <script type="text/javascript" src="assets/js/topMenu.js"></script>

    <title> <?php echo $companyName . ' - ' . $pageTitle; ?> </title>
    <link rel='stylesheet' href='assets/css/border.css'/>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link rel='stylesheet' href='assets/css/floatNav.css'/>

  </head>
  <body>
    <div class='page'>
        <div class="navigation">
            <?php

            require_once ('/assets/inc/menu.php');

            ?>
        </div>
        <?php
            require_once('/assets/inc/header.php');
            require_once('/assets/inc/leftPane.php');
            require_once('/assets/inc/rightPane.php');

        ?>
        <div class='content'>
            <div class="container">
                This is the body!
            </div>
        </div>
    </div>

        <?php

            require_once('/assets/inc/footer.php');
        ?>
  </body>
</html>